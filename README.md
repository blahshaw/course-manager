# Coursera Front End Take Home Assignment

This is a React Application hosted on an ExpressJS server.

## Prerequisites

1. You must have [NPM](https://docs.npmjs.com/getting-started/installing-node) installed on your machine (Running npm install shouldn't be necessary).

## Deployment

1. Extract the project from the .zip file
2. Navigate the root project directory.
1. In the project root directory, run `node server.js`. Visit http://localhost:8080 to view the list of courses and add/drop the courses. You can specify which port the server runs on server.js in the root directory of the project.
